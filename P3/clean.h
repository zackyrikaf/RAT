#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>

#define SIZEBUFFER 1024
#define SERVEURNAME "127.0.0.1" // Ip interne

void CleanWrite(int socket, void *msg){
    int total_envoye=0,envoi;
    
    /* Envoie de la taille du fichier */
    int taille_msg=sizeof(msg);
    int taille_rest=taille_msg;
    write(socket, &taille_msg, sizeof(taille_msg));
    
    /* Envoie du fichier */
    while(total_envoye<taille_msg)
    {
        if((envoi = write(socket, msg, (taille_rest>=SIZEBUFFER)?SIZEBUFFER:taille_rest)) == -1) // Write prend en parametre la socket, ce qui doit etre envoye et sa taille
        {
            perror("Erreur lors du nb d'octets envoyé !");
        }
        taille_rest-=envoi;
        total_envoye+=envoi;
    }
}

void CleanRead(int socket, void *buffer){
    int total_lu=0,taille_msg,lu;
    
    /* Réception de la taille du fichier */
    /* read lit sur la socket mise en parametre, elle prend en parametre la socket que l'on veut lire, la variable a remplir et la taille de cette variable */
    read(socket, &taille_msg, sizeof(taille_msg));
    int taille_rest=taille_msg;
    
    /* Réception du fichier */
    while(total_lu<taille_msg)
    {
        if ((lu = read(socket, buffer, (taille_rest>=SIZEBUFFER)?SIZEBUFFER:taille_rest)) == -1) //lu est le nb d'octets lu max 1024
        {
            perror("Erreur lors du nb d'octets lu !");
        }
        taille_rest-=lu;
        total_lu+=lu;
    }
}

//SERVEUR:
//printf(">>Entrez votre message: \n");
//scanf("%s",msg);
//printf("%s \n",msg);
/*char msg[1024];
 char *m = msg;
 size_t msgl = sizeof(msg);
 */
//getline(&m,&msgl,stdin);
//fgets(msg, sizeof(msg), stdin);
//read(0, msg, 8200);
//scanf deconseillé
//write(client_socket,msg, sizeof(msg));

/*
 while (strcmp(msg, "exit") != 0)
 {
 printf("Entrez votre message: \n");
 //getline(msg,sizeof(msg),stdin);
 write(client_socket,msg, sizeof(msg));
 usleep(10);
 }
 */
