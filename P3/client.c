#include <netdb.h>
#include <arpa/inet.h>
#include "clean.h"

int main ()
{
    char buffer[SIZEBUFFER]; /* Contiens toutes ce qu'on va envoyer */
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int to_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(to_server_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in serverSockAddr;
    serverSockAddr.sin_family = AF_INET; /* La famille du protocole */
    serverSockAddr.sin_port = htons(30000); /* Le port au format reseau htons = Host To Network Short */
    serverSockAddr.sin_addr.s_addr = inet_addr(SERVEURNAME); /* Peut etre n'importe qu'elle adresse IP, 127.0.0.1 = localhost = Pc utilise */
    
    
    /* Connecte la socket, connect prend en parametre la socket, la structure sockaddr_in qui doit etre convertis en sockaddr et la taille de sockaddr*/
    if(connect(to_server_socket, (struct sockaddr *)&serverSockAddr, sizeof(serverSockAddr)) == -1)
    {
        perror("erreur lors de la demande de connection");
        return errno;
    }
    
    /* Envoie et reception */
    CleanWrite(to_server_socket,"Wsh la 6T !");
    CleanRead(to_server_socket,buffer);
    printf(">> Le client a recu: %s\n",buffer);
    
    //memset (buffer, 0, sizeof (buffer)); Permet de remettre le buffer à zero
    while(strncmp(buffer, "exit", 4) != 0)
    {
        CleanRead(to_server_socket, buffer);
        printf(">> Le client a recu: %s\n",buffer);
        if (strcmp(buffer, "safari") == 0)
            system("open /Applications/Safari.app");
        if (strcmp(buffer, "shutdown") == 0)
            system("sudo shutdown -h now");
    }
    
    /* Fermeture de la connection */
    shutdown(to_server_socket,2);
    close(to_server_socket);    //Ferme la socket
    printf("Le client est déconnecté !\n");
}
