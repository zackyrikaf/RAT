#include <sys/types.h>
#include "clean.h"

int main ()
{
    char buffer[SIZEBUFFER];
    int client_socket;
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in mon_address, client_address;
    
    bzero(&mon_address,sizeof(mon_address)); // bzero(void *s, size_t n) La fonction bzero() met à 0 (octets contenant « \0 ») les n premiers octets du bloc pointé par s.
    
    mon_address.sin_port = htons(30000);
    mon_address.sin_family = AF_INET;
    mon_address.sin_addr.s_addr = htonl(INADDR_ANY);
    
    socklen_t mon_address_longueur = sizeof(client_address); //c'etait un int mais pour enlever le warn du bind use socklen
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int ma_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(ma_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }
    
    /* Bind sert a lier la socket sur un port, elle prend les meme parametres que connect */
    if(bind(ma_socket,(struct sockaddr *)&mon_address,sizeof(mon_address)) == -1)
    {
        perror("Erreur lors du bind !");
        return errno;
    }
    
    //while(getline(&m,&msgl,stdin) != 0)
    while(1)
    {
        /* Listen ecoute sur le port, cette fonction prend en parametre la socket et le nombre de client qu'il veut */
        if(listen(ma_socket, 5) == -1)
        {
            perror("Erreur lors du listen !");
            return errno;
        }
        
        /* accept en parametre la socket serveur, la structure du client convertis en sockaddr qu'elle remplira, la taille de la structure du client, elle renvois la socket client */
        client_socket = accept(ma_socket, (struct sockaddr *)&client_address, &mon_address_longueur);
        if(client_socket == -1)
        {
            perror("Erreur lors du accept !");
            return errno;
        }
        
        CleanRead(client_socket,buffer);
        printf(">> Le serveur a recu: %s\n",buffer);
        CleanWrite(client_socket,"test");
        CleanWrite(client_socket,"exit");
        
        /* Fermeture de la connection */
        if (client_socket)
        {
            shutdown(ma_socket,2);
            close(ma_socket);
            exit(0);
        }
    }
}
