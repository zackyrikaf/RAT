#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

char buffer[1024];
int ma_socket;

int main ()
{
    int client_socket; /* La socket du client */
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in mon_address, client_address;
    
    bzero(&mon_address,sizeof(mon_address)); // bzero(void *s, size_t n) La fonction bzero() met à 0 (octets contenant « \0 ») les n premiers octets du bloc pointé par s.
    
    mon_address.sin_port = htons(30000);
    mon_address.sin_family = AF_INET;
    mon_address.sin_addr.s_addr = htonl(INADDR_ANY);

    socklen_t mon_address_longueur = sizeof(client_address); //c'etait un int mais pour enlever le warn du bind use socklen
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int ma_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(ma_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }

    /* Bind sert a lier la socket sur un port, elle prend les meme parametres que connect */
    if(bind(ma_socket,(struct sockaddr *)&mon_address,sizeof(mon_address)) == -1)
    {
        perror("Erreur lors du bind !");
        return errno;
    }
    
    
    while(1)
    {
        /* Listen ecoute sur le port, cette fonction prend en parametre la socket et le nombre de client qu'il veut */
        if(listen(ma_socket, 5) == -1)
        {
                perror("Erreur lors du listen !");
            return errno;
        }
    
        /* accept en parametre la socket serveur, la structure du client convertis en sockaddr qu'elle remplira, la taille de la structure du client, elle renvois la socket client */
        client_socket = accept(ma_socket, (struct sockaddr *)&client_address, &mon_address_longueur);
        if(client_socket == -1)
        {
            perror("Erreur lors du accept !");
            return errno;
        }

        /* read lit sur la socket mise en parametre, elle prend en parametre la socket que l'on veut lire, la variable a remplir et la taille de cette variable */
        if(read(client_socket,buffer, 512) == -1)
        {
            perror("Erreur lors du read !");
            return errno;
        }
            
        printf("Le serveur a recu: %s\n",buffer);
        write(client_socket,"Bye bye bg du 91 !", 512);
        shutdown(client_socket,2);
        close(client_socket);
    }
    
    /* Fermeture de la connection */
    shutdown(ma_socket,2);
    close(ma_socket);
}
