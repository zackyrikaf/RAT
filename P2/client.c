#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SERVEURNAME "127.0.0.1" // Ip interne

int to_server_socket = -1;

int main ()
{
    char buffer[1024]; /* Contiens toutes ce qu'on va envoyer */
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int to_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(to_server_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in serverSockAddr;
    serverSockAddr.sin_family = AF_INET; /* La famille du protocole */
    serverSockAddr.sin_port = htons(30000); /* Le port au format reseau htons = Host To Network Short */
    serverSockAddr.sin_addr.s_addr = inet_addr(SERVEURNAME); /* Peut etre n'importe qu'elle adresse IP, 127.0.0.1 = localhost = Pc utilise */
    
    
    /* Connecte la socket, connect prend en parametre la socket, la structure sockaddr_in qui doit etre convertis en sockaddr et la taille de sockaddr*/
    if(connect(to_server_socket, (struct sockaddr *)&serverSockAddr, sizeof(serverSockAddr)) == -1)
    {
        perror("erreur lors de la demande de connection");
        return errno;
    }

    /* Envoie et reception */
    write(to_server_socket,"Wsh la 6T !",11); // Write prend en parametre la socket, ce qui doit etre envoye et sa taille
    
    while(strcmp(buffer, "exit") != 0) //ou pas de msg (opt)
    {
        read(to_server_socket,buffer,sizeof(buffer));
        printf("Le client a recu: %s\n",buffer);
    }
    
    /* Fermeture de la connection */
    shutdown(to_server_socket,2);
    close(to_server_socket);    //Ferme la socket
    printf("Le client est déconnecté !\n");
}
