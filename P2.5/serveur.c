#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SIZEBUFFER 1024

int main (void)
{
    char buffer[SIZEBUFFER];
    int client_socket;
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in mon_address, client_address;
    
    bzero(&mon_address,sizeof(mon_address)); // bzero(void *s, size_t n) La fonction bzero() met à 0 (octets contenant « \0 ») les n premiers octets du bloc pointé par s.
    
    mon_address.sin_port = htons(30000);
    mon_address.sin_family = AF_INET;
    mon_address.sin_addr.s_addr = htonl(INADDR_ANY);

    socklen_t mon_address_longueur = sizeof(client_address); //c'etait un int mais pour enlever le warn du bind use socklen
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int ma_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(ma_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }

    /* Bind sert a lier la socket sur un port, elle prend les meme parametres que connect */
    if(bind(ma_socket,(struct sockaddr *)&mon_address,sizeof(mon_address)) == -1)
    {
        perror("Erreur lors du bind !");
        return errno;
    }
    
    
    while(1)
    {
        /* Listen ecoute sur le port, cette fonction prend en parametre la socket et le nombre de client qu'il veut */
        if(listen(ma_socket, 5) == -1)
        {
                perror("Erreur lors du listen !");
            return errno;
        }
    
        /* accept en parametre la socket serveur, la structure du client convertis en sockaddr qu'elle remplira, la taille de la structure du client, elle renvois la socket client */
        client_socket = accept(ma_socket, (struct sockaddr *)&client_address, &mon_address_longueur);
        if(client_socket == -1)
        {
            perror("Erreur lors du accept !");
            return errno;
        }
        
        //Ouverture du fichier qui sera envoyé
        int file,taille,fin=0,te,envoi,lu;
        
        if((file=open("image.jpg",O_RDONLY))!=-1)
        {
            printf("L'ouverture du fichier serveur a reussi !\n");
            
            //Envoi de la taille du fichier
            taille=lseek(file, 0, SEEK_END);
            lseek(file, 0, SEEK_SET);
            //printf("Taille du fichier : %d octets.\n", taille);
            write(client_socket, &taille, sizeof(taille));
            
            /* Envoie du fichier */
            while(fin<taille)
            {
                if((lu = read(file, &buffer, SIZEBUFFER)) == -1)
                {
                    perror("Erreur lors du nb d'octets lu !");
                    return errno;
                }
                
                if((envoi = write(client_socket, &buffer, lu)) == -1)
                {
                    perror("Erreur lors du nb d'octets envoyé !");
                    return errno;
                }
                
                te+=envoi;
                fin+=lu;
            }
        }
        printf("\nNombre d'octets total envoyes : %d octets\n\n", te);
        
        write(client_socket,"exit", 5);
    }
    
    /* Fermeture de la connection */
    shutdown(ma_socket,2);
    close(ma_socket);
}
