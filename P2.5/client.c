#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SERVEURNAME "127.0.0.1" // Ip interne
#define SIZEBUFFER 1024

int main ()
{
    char buffer[SIZEBUFFER]; /* Contiens toutes ce qu'on va envoyer */
    
    /* Cree une socket, socket prend en parametre le domaine, le type et le protocole */
    int to_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(to_server_socket == -1)
    {
        perror("Erreur lors de la création socket serveur !");
        return errno;
    }
    
    /* Cree une structure qui contiendras les informations de la socket */
    struct sockaddr_in serverSockAddr;
    serverSockAddr.sin_family = AF_INET; /* La famille du protocole */
    serverSockAddr.sin_port = htons(30000); /* Le port au format reseau htons = Host To Network Short */
    serverSockAddr.sin_addr.s_addr = inet_addr(SERVEURNAME); /* Peut etre n'importe qu'elle adresse IP, 127.0.0.1 = localhost = Pc utilise */
    
    
    /* Connecte la socket, connect prend en parametre la socket, la structure sockaddr_in qui doit etre convertis en sockaddr et la taille de sockaddr*/
    if(connect(to_server_socket, (struct sockaddr *)&serverSockAddr, sizeof(serverSockAddr)) == -1)
    {
        perror("erreur lors de la demande de connection");
        return errno;
    }

    /* Reception du fichier */
    
    int file,taille,fin=0,filesize,envoi,lu,ecrit,te;
    
    if((file=open("image_recu.jpg", O_WRONLY|O_CREAT|O_TRUNC|S_IREAD))!=-1)
    {
        printf("L'ouverture du fichier client a reussi !\n");
        
        //Réception de la taille du fichier
        read(to_server_socket, &filesize, sizeof(filesize));
        //printf("Taille du fichier : %d octets\n", filesize);
        
        //Reception du fichier envoye par le client et ecriture du fichier
        int tr=filesize;
        while(fin<filesize)
        {
            if ((lu=read(to_server_socket, &buffer, (tr>=SIZEBUFFER)?SIZEBUFFER:tr)) == -1) //lu est le nb d'octets lu
            {
                perror("Erreur lors du nb d'octets lu !");
                return errno;
            }
            if((ecrit=write(file, &buffer, lu)) == -1) //ecrit est le nb d'octets ecrits
            {
                perror("Erreur lors du nb d'octets ecrits !");
                return errno;
            }
            te+=ecrit;
            tr-=lu;
            fin+=lu;
        }
        printf("Nombre d'octets total ecrits : %d octets\n", te);
        printf("Ecriture terminee !\n");
    }
    
    //memset (buffer, 0, sizeof (buffer)); Permet de remettre le buffer à zero
    
    while(strncmp(buffer, "exit", 4) != 0) 
    {
        read(to_server_socket, buffer, SIZEBUFFER);
        printf(">> Le client a recu: %s\n",buffer);
        if (strcmp(buffer, "safari") == 0)
            system("open /Applications/Safari.app");
        if (strcmp(buffer, "shutdown") == 0)
            system("sudo shutdown -h now");
    }
    
    /* Fermeture de la connection */
    shutdown(to_server_socket,2);
    close(to_server_socket);    //Ferme la socket
    printf("Le client est déconnecté !\n");
}
